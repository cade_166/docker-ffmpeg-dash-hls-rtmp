#!/bin/bash
if [ $(id -u) -ne 0 ]; then
  echo "Execute with root user."
  echo "You can use sudo, su, sudo su, su -"
  exit 1
fi

user=$(whoami)
file=$(ls | find . -type f | grep .mp4 | sed -n 1p)
docker run --rm -e URL_RTMPS=rtmp:localhost/live/c11 -p 1935:1935 -v file:/usr/app/input.mp4 -d $user/ffmpeg:1.0
