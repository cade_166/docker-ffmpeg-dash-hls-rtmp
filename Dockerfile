FROM ubuntu:20.04

RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install \
            build-essential \
            git \
            libpcre3-dev \
            libssl-dev \
            zlib1g-dev \
            ffmpeg &&\
            mkdir rtmp && \
            cd rtmp && \
            git clone https://github.com/arut/nginx-rtmp-module.git && \
            git clone https://github.com/nginx/nginx.git && \
            cd nginx && \
            ./auto/configure --add-module=../nginx-rtmp-module && \
            make && \
            make install
RUN echo "events { }   rtmp {   server {   listen 1935;   application live {   live on;   interleave on;   hls on;   hls_path /tmp/hls;   hls_fragment 15s;   dash on;   dash_path /tmp/dash;   dash_fragment 15s;   }   }   }   http {   default_type application/octet-stream;   server {   listen 80;   location / {   root /tmp;   }   types {   application/vnd.apple.mpegurl m3u8;   video/mp2t ts;   text/html html;   application/dash+xml mpd;   }   }   }  " > /usr/local/nginx/conf/nginx.conf && \
            /usr/local/nginx/sbin/nginx -t && \
            /usr/local/nginx/sbin/nginx && \
            echo "#!/bin/sh" > /etc/init.d/nginx.sh && \
            echo "/usr/local/nginx/sbin/nginx" >> /etc/init.d/nginx.sh && \
            chmod +x /etc/init.d/nginx.sh && \
            update-rc.d /etc/init.d/nginx.sh defaults

WORKDIR /usr/app
ENV URL_RTMPS=""
CMD /usr/local/nginx/sbin/nginx && ffmpeg \
    -re \
    -i input.mp4 \
    -vcodec copy \
    -loop -1 \
    -c:a aac \
    -b:a 160k \
    -ar 44100 \
    -strict \
    -2 \
    -f flv \
    $URL_RTMPS
