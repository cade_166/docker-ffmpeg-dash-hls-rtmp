#!/bin/bash
if [ $(id -u) -ne 0 ]; then
  echo "Execute with root user."
  echo "You can use sudo, su, sudo su, su -"
  exit 1
fi
docker network create -d bridge \
    --subnet=240.0.0.0/24 \
    --gateway=240.0.0.1 \
    sl-network
